import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './orders/orders.component';
import { OrderComponent } from './orders/order/order.component';
import { Order } from './shared/order.model';
import { OrderItemsComponent } from './orders/order-items/order-items.component';
import { CustomerComponent } from './orders/customer/customer.component';
import { CreateHotDogComponent } from './orders/create-hot-dog/create-hot-dog.component';
import { ActualOrderItemComponent } from './orders/actual-order-item/actual-order-item.component';
import { UserComponent } from './orders/user/user.component';
import { CreateOrderItemComponent } from './orders/create-order-item/create-order-item.component';
import { AddUserComponent } from './orders/add-user/add-user.component';
import { AddCustomerComponent } from './orders/add-customer/add-customer.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';
import { LoginPageComponent } from './orders/login-page/login-page.component';
import { HomePageComponent } from './orders/home-page/home-page.component';








const routes: Routes = [
 
  {path:'orders',component:OrdersComponent},
  {path:'createHotDog',component:CreateHotDogComponent},
  {path:'customer',component:CustomerComponent},
  {path:'order-items',component:OrderItemsComponent},
  {path: 'actual-order-item', component:ActualOrderItemComponent},
  {path: 'create-order-item', component:CreateOrderItemComponent},
  {path: 'add-user', component:AddUserComponent},
  {path: 'add-customer', component:AddCustomerComponent},
  {path: 'add-order', component:AddOrderComponent},
  {path: 'user', component:UserComponent},
  {path: 'login', component:LoginPageComponent},
  {path: 'homepage', component:HomePageComponent},
  {path:'order',children:[
    {path:'',component:OrderComponent}, 
    {path:'edit/:id',component:OrderComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
