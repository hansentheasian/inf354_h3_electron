import { Component, OnInit } from '@angular/core';
import{CustomerService} from 'src/app/shared/customer.service';
import { Customer } from 'src/app/shared/customer.model';
import { Order } from 'src/app/shared/order.model';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
formData:Customer;
CustomerList:Customer[];
  constructor(
   public router:Router,
    public CustomerService:CustomerService
  ) { }

  ngOnInit(){

    this.CustomerService.getCustomerList().then(res => this.CustomerList = res as Customer[]);

    this.formData={
      CustomerID:0,
      Name:null,
      Surname:null
    
    }
  }
  
  HomePage(){
    this.router.navigate(['/homepage']);
  }
  DeleteCustomer(id:number){
    this.CustomerService.DeleteCustomer(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }

  resetForm() {
    this.formData = {
      CustomerID:0,
      Name:null,
      Surname:null,
    }
  }

  refreshList() {
    this.CustomerService.getCustomerList().then(res => this.CustomerList = res as Customer[]);
  }

  AddCustomer(){
    this.router.navigate(['/add-customer']);
  }

  SaveCustomer() {
    this.CustomerService.UpdateCustomer(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  EditCustomer(hdObj : Customer){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.CustomerID = hdObj.CustomerID;
    this.formData.Name = hdObj.Name;
    this.formData.Surname = hdObj.Surname;
   
  }

}
