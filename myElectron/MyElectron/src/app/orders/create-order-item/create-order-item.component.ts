import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import{FormBuilder, Validators, ReactiveFormsModule, Form, FormGroup} from '@angular/forms';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { Orderitem } from 'src/app/shared/orderitem.model';

@Component({
  selector: 'app-create-order-item',
  templateUrl: './create-order-item.component.html',
  styleUrls: ['./create-order-item.component.css']
})
export class CreateOrderItemComponent implements OnInit {

  constructor(
    public OrderItemService:HotdogService,
    public router:Router,
   public formBuilder:FormBuilder,
  ) { }

  MyForm:any;
  data = false;
  message : string;
  ngOnInit(): void {
    
    this.MyForm = this.formBuilder.group({
      OrderItemID:['',[Validators.required]],
      OrderID:['',[Validators.required]],
      HotDogID:['',[Validators.required]],
      Quantity:['',[Validators.required]],
    })

    this.OrderItemService.formData2={
      OrderItemID:0,
      OrderID:0,
      HotDogID:0,
      Quantity:0,
  }
}
SaveOrderItem(){
  console.log(this.OrderItemService.formData2);
  this.OrderItemService.AddOrderItem(this.OrderItemService.formData2).subscribe(res => {
    console.log(res);
  })
}

CreateOrderItem(orderitem:Orderitem){
  console.log(Orderitem);
  this.OrderItemService.AddOrderItem(this.OrderItemService.formData2).subscribe(()=>{
    this.data = true;
    this.message = 'Saved';
    this.MyForm.reset();
  })
}
GoBackToList(){
  this.router.navigate(['/actual-order-item'])
}


}
