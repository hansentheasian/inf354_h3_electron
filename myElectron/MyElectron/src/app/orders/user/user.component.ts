import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/user';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  formData:User;
  UserList:User[];
  
  constructor(
    private UserService:HotdogService,
    public router: Router
  ) { }
  HomePage(){
    this.router.navigate(['/homepage']);
  }

  resetForm() {
    this.formData = {
      UserID:0,
      Name:null,
      Surname:null,
      Email:null,
      Password:null,
    }
  }

  refreshList() {
    this.UserService.getUserList().then(res => this.UserList = res as User[]);
  }

  ngOnInit(): void {

  

      this.UserService.getUserList().then(res => this.UserList = res as User[]);
  
      this.formData={
        UserID:0,
        Name:null,
        Surname:null,
        Email:null,
        Password:null
      }

      
  }
  DeleteUser(id:number){
    this.UserService.DeleteUser(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }

  AddUser(){
    this.router.navigate(['/add-user']);
  }
  SaveUser() {
    this.UserService.UpdateUser(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  EditUser(hdObj : User){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.UserID = hdObj.UserID;
    this.formData.Name = hdObj.Name;
    this.formData.Surname = hdObj.Surname;
    this.formData.Email = hdObj.Email;
    this.formData.Password = hdObj.Password;
  }

}
