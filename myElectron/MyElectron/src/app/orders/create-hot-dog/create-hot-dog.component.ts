import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import{FormBuilder, Validators, ReactiveFormsModule} from '@angular/forms';
import { Item } from 'src/app/shared/item.model';
import { HotdogService } from 'src/app/shared/hotdog.service';

@Component({
  selector: 'app-create-hot-dog',
  templateUrl: './create-hot-dog.component.html',
  styleUrls: ['./create-hot-dog.component.css']
})
export class CreateHotDogComponent implements OnInit {
  
  

  constructor(
    private formbuilder: FormBuilder,
    private router : Router,
    public hotdogservice:HotdogService
  ) { }

  HotDogForm:any;
  data = false;
  message : string;
  ngOnInit(): void {

    this.HotDogForm = this.formbuilder.group({
      HotDogName:['',[Validators.required]],
      HotDogPrice:['',[Validators.required]]
    })
    this.hotdogservice.formData={
      HotDogID:0,
      Name : '',
      Price: 0
    }
  }
  SaveHotDog(){
    console.log(this.hotdogservice.formData);
    this.hotdogservice.AddHotDog(this.hotdogservice.formData).subscribe(res => {
      console.log(res);
    })
  }
  onFormSubmit(HotDogForm){
    const hotdog = HotDogForm.value;
    this.CreateHotDog(hotdog)
  }

  CreateHotDog(hotdog : Item){
    this.hotdogservice.AddHotDog(hotdog).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.HotDogForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/order-items'])
  }

  
}// export
