import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHotDogComponent } from './create-hot-dog.component';

describe('CreateHotDogComponent', () => {
  let component: CreateHotDogComponent;
  let fixture: ComponentFixture<CreateHotDogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHotDogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHotDogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
