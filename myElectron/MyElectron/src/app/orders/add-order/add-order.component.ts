import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { Order } from 'src/app/shared/order.model';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {

  
  constructor(
    private formbuilder: FormBuilder,
    private router : Router,
    public OrderService:HotdogService
  ) { }
  OrderForm:any;
  data = false;
  message : string;
  ngOnInit(): void {

    this.OrderForm = this.formbuilder.group({
      OrderID:['',[Validators.required]],
      OrderNo:['',[Validators.required]],
      CustomerID:['',[Validators.required]],
      PayMethod:['',[Validators.required]],
    })
    this.OrderService.formData3={
      OrderID:0,
      OrderNo:null,
      CustomerID:0,
      PayMethod:null,

      
    }
  }
  SaveOrder(){
    console.log(this.OrderService.formData3);
    this.OrderService.AddOrder(this.OrderService.formData3).subscribe(res => {
      console.log(res);
    })
  }
  onFormSubmit(OrderForm){
    const order = OrderForm.value;
    this.CreateOrder(order)
  }

  CreateOrder(order: Order){
    this.OrderService.AddOrder(order).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.OrderForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/order'])
  }

  

}