import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/shared/order.model';
import { OrderService } from 'src/app/shared/order.service';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
formData:Order;
OrderList: Order[];
  constructor(
    public router:Router,
    public OrderService: HotdogService) { }

  ngOnInit():void{
    
    this.OrderService.getOrderList().then(res => this.OrderList = res as Order[]);

    this.formData={
      OrderID:0,
      OrderNo:'',
      CustomerID:0,
      PayMethod:''
    }
  }
  HomePage(){
    this.router.navigate(['/homepage']);
  }
  DeleteOrder(id:number){
    this.OrderService.DeleteOrder(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }

  resetForm() {
    this.formData = {
      OrderID:0,
      OrderNo:null,
      CustomerID:0,
      PayMethod:null,
    }
  }

  refreshList() {
    this.OrderService.getOrderList().then(res => this.OrderList = res as Order[]);
  }
  AddOrder(){
    this.router.navigate(['/add-order']);
  }
  SaveOrder() {
    this.OrderService.UpdateOrder(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  EditOrder(hdObj : Order){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.OrderID = hdObj.OrderID;
    this.formData.OrderNo = hdObj.OrderNo;
    this.formData.CustomerID = hdObj.CustomerID;
    this.formData.PayMethod = hdObj.PayMethod;
   
  }
}
