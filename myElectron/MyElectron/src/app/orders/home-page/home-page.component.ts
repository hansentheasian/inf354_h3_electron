import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(
    public router:Router
  ) { }

  ngOnInit(): void {
  }

  OrderItems(){
    this.router.navigate(['/actual-order-item'])
  }

  HotDogs(){
    this.router.navigate(['/order-items'])
  }
  Users(){
    this.router.navigate(['/user'])
  }
  Customers(){
    this.router.navigate(['/customer'])
  }
  Orders(){
    this.router.navigate(['/order'])
  }
}
