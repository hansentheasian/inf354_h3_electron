import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/shared/customer.service';
import { Customer } from 'src/app/shared/customer.model';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {

  constructor(
    public formBuilder:FormBuilder,
    public router:Router,
    public CustomerService:CustomerService
  ) { }

  CustomerForm:any;
  data = false;
  message : string;

  ngOnInit(): void {
    this.CustomerForm = this.formBuilder.group({
      CustomerName:['',[Validators.required]],
      CustomerSurname:['',[Validators.required]]
    })
    this.CustomerService.formData={
      CustomerID:0,
      Name : null,
      Surname:null
    }
  }
  SaveCustomer(){
    console.log(this.CustomerService.formData);
    this.CustomerService.AddCustomer(this.CustomerService.formData).subscribe(res => {
      console.log(res);
    })
  }

  onFormSubmit(CustomerForm){
    const customer = CustomerForm.value;
    this.CreateCustomer(customer)
  }
  CreateCustomer(customer:Customer){
    this.CustomerService.AddCustomer(customer).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.CustomerForm.reset();
    })
  }

  GoBackToList(){
    this.router.navigate(['/customer'])
  }
}
