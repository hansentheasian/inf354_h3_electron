import { Component, OnInit, Inject } from '@angular/core';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { Item } from 'src/app/shared/item.model';
import { Orderitem } from 'src/app/shared/orderitem.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-order-items',
  templateUrl: './order-items.component.html',
  styleUrls: ['./order-items.component.css']
})
export class OrderItemsComponent implements OnInit {
  formData:Item;
  HotDogList: Item[];
  constructor(
  public HotDogService:HotdogService,
  private router : Router,
  ) { }

  ngOnInit(){
    this.resetForm();
    this.refreshList();
    // this.HotDogService.getItemList().subscribe
    
  }// ngOnInit
  HomePage(){
    this.router.navigate(['/homepage']);
  }
  resetForm() {
    this.formData = {
      HotDogID: 0,
      Name: '',
      Price: 0
    }
  }

  refreshList() {
    this.HotDogService.getItemList().then(res => this.HotDogList = res as Item[]);
  }

  AddHotDog(){
    this.router.navigate(['/createHotDog'])
  }
  DeleteHotDog(id:number){
    this.HotDogService.DeleteHotDog(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }
  EditHotDog(hdObj : Item){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.HotDogID = hdObj.HotDogID;
    this.formData.Name = hdObj.Name;
    this.formData.Price = hdObj.Price;
  }

  SaveHotDog() {
    this.HotDogService.UpdateHotDog(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }

} // Export
