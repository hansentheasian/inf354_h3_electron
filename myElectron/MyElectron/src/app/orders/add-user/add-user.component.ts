import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { User } from 'src/app/shared/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(
    private formbuilder: FormBuilder,
    private router : Router,
    public UserService:HotdogService
  ) { }
  UserForm:any;
  data = false;
  message : string;
  ngOnInit(): void {

    this.UserForm = this.formbuilder.group({
      UserName:['',[Validators.required]],
      UserSurname:['',[Validators.required]],
      Email:['',[Validators.required]],
      Password:['',[Validators.required]],
    })
    this.UserService.formData5={
      UserID:0,
      Name:null,
      Surname:null,
      Email:null,
      Password:null,
      
    }
  }
  SaveUser(){
    console.log(this.UserService.formData);
    this.UserService.AddUser(this.UserService.formData5).subscribe(res => {
      console.log(res);
    })
  }
  onFormSubmit(UserForm){
    const User = UserForm.value;
    this.CreateHotDog(User)
  }

  CreateHotDog(user: User){
    this.UserService.AddUser(user).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.UserForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/user'])
  }

  

}
