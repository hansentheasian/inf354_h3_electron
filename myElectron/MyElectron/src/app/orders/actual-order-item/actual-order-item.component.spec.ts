import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualOrderItemComponent } from './actual-order-item.component';

describe('ActualOrderItemComponent', () => {
  let component: ActualOrderItemComponent;
  let fixture: ComponentFixture<ActualOrderItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualOrderItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualOrderItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
