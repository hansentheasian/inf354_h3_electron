import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/shared/order.model';
import { HotdogService } from 'src/app/shared/hotdog.service';
import { Orderitem } from 'src/app/shared/orderitem.model';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-actual-order-item',
  templateUrl: './actual-order-item.component.html',
  styleUrls: ['./actual-order-item.component.css']
})
export class ActualOrderItemComponent implements OnInit {
formData:Orderitem;
  OrderItemList:Orderitem[];
  constructor(
    private OrderItemService:HotdogService,
    private router:Router,
   private formBuilder:FormBuilder,
  ) { }
  
  ngOnInit(): void {

    
    this.OrderItemService.getOrderItemList().then(res => this.OrderItemList = res as Orderitem[]);

    this.formData={
      OrderItemID:0,
      OrderID:0,
      HotDogID:0,
      Quantity:0
    }
  }
  HomePage(){
    this.router.navigate(['/homepage']);
  }
  AddOrderItem(){
    this.router.navigate(['/create-order-item'])
  }

  DeleteOrderItem(id:number){
    this.OrderItemService.DeleteOrderItem(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }

  resetForm() {
    this.formData = {
     OrderItemID:0,
     OrderID:0,
     HotDogID:0,
      Quantity:0,

    }
  }

  refreshList() {
    this.OrderItemService.getOrderItemList().then(res => this.OrderItemList = res as Orderitem[]);
  }

  SaveOrderItem() {
    this.OrderItemService.UpdateOrderItem(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  EditOrderItem(hdObj : Orderitem){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.OrderItemID = hdObj.OrderItemID;
    this.formData.OrderID = hdObj.OrderID;
    this.formData.HotDogID = hdObj.HotDogID;
    this.formData.Quantity = hdObj.Quantity;
    
   
  }

}
