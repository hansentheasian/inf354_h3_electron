import { TestBed } from '@angular/core/testing';

import { HotdogService } from './hotdog.service';

describe('HotdogService', () => {
  let service: HotdogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HotdogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
