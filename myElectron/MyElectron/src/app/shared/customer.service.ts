import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Customer } from './customer.model';
import { Order } from './order.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  formData:Customer;

  constructor(private http: HttpClient) { }

  getCustomerList() {

    return this.http.get(environment.apiURL + '/Customer/GetCustomers').toPromise();
  }

  DeleteCustomer(id: number) {
    return this.http.delete(environment.apiURL + '/Customer/' + id)
  }


  AddCustomer(newCustomer: Customer) {
    return this.http.post(environment.apiURL + '/Customer/PostCustomer', newCustomer);
  }

  UpdateCustomer(newCustomer: Customer) {
    return this.http.put(environment.apiURL + '/Customer/' + newCustomer.CustomerID, newCustomer);
  }
}
