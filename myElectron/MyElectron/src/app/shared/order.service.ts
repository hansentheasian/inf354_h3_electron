import { Injectable } from '@angular/core';
import { Order } from './order.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
formData:Order;

  constructor(private http:HttpClient) { }

  getOrderList(){

    return this.http.get(environment.apiURL + '/Order/GetOrders').toPromise();
  }
}
