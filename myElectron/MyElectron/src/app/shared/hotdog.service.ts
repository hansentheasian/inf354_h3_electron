import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Order } from './order.model';
import { Item } from './item.model';
import { Orderitem } from './orderitem.model';
import { Customer } from './customer.model';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class HotdogService {

  formData:Item;
  formData2:Orderitem;
  formData3:Order;
  formData4:Customer;
  formData5:User;

  constructor(private http:HttpClient) { }

  getItemList(){
  return this.http.get(environment.apiURL+'/HotDog/GetHotDogs').toPromise();
  }
  AddHotDog(newHotDog : Item){
   return this.http.post(environment.apiURL + '/HotDog/PostHotDog',newHotDog);
  }
  UpdateHotDog(newHotDog:Item){
    return this.http.put(environment.apiURL+'/HotDog/' + newHotDog.HotDogID, newHotDog);
  }
  DeleteHotDog(id:number){
    return this.http.delete(environment.apiURL+'/HotDog/'+id);
  }

  getOrderList(){
    return this.http.get(environment.apiURL+'/Order/GetOrders').toPromise();
  }

  getOrderItemList(){
    return this.http.get(environment.apiURL+'/OrderItem/GetOrderItems').toPromise();
  }
  getUserList(){
    return this.http.get(environment.apiURL +'/User/GetUsers').toPromise();
  }
  DeleteUser(id:number){
    return this.http.delete(environment.apiURL+'/User/' + id);
  }
  AddUser(newUser:User){
    return this.http.post(environment.apiURL+'/User/PostUser', newUser);
  }
  AddOrderItem(newOrderItem:Orderitem){
    return this.http.post(environment.apiURL + '/OrderItem/PostOrderItem', newOrderItem);
  }

  UpdateUser(newUser:User){
    return this.http.put(environment.apiURL+'/User/' + newUser.UserID, newUser);
  }

 
  AddOrder(newOrder : Order){
    return this.http.post(environment.apiURL + '/Order/PostOrder',newOrder);
   }
   DeleteOrder(id:number){
    return this.http.delete(environment.apiURL+'/Order/' + id);
  }

  UpdateOrder(newOrder:Order){
    return this.http.put(environment.apiURL+'/Order/' + newOrder.OrderID, newOrder);
  }

  DeleteOrderItem(id:number){
    return this.http.delete(environment.apiURL+'/OrderItem/' + id);
  }

  UpdateOrderItem(newOrderItem:Orderitem){
    return this.http.put(environment.apiURL+'/OrderItem/' + newOrderItem.OrderItemID, newOrderItem);
  }
   
}
