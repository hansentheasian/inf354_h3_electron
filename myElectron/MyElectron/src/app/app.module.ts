import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderComponent } from './orders/order/order.component';
import { OrderItemsComponent } from './orders/order-items/order-items.component';
import {HttpClientModule} from '@angular/common/http';
import { OrderService } from './shared/order.service';
import { CustomerComponent } from './orders/customer/customer.component';
import { CreateHotDogComponent } from './orders/create-hot-dog/create-hot-dog.component';
import { ActualOrderItemComponent } from './orders/actual-order-item/actual-order-item.component';
import { UserComponent } from './orders/user/user.component';
import { CreateOrderItemComponent } from './orders/create-order-item/create-order-item.component';
import { AddUserComponent } from './orders/add-user/add-user.component';
import { AddCustomerComponent } from './orders/add-customer/add-customer.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';
import { HomePageComponent } from './orders/home-page/home-page.component';
import { LoginPageComponent } from './orders/login-page/login-page.component';
import { Router } from '@angular/router';






@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent,
    OrderComponent,
    OrderItemsComponent,
    CustomerComponent,
    CreateHotDogComponent,
    ActualOrderItemComponent,
    UserComponent,
    CreateOrderItemComponent,
    AddUserComponent,
    AddCustomerComponent,
    AddOrderComponent,
    LoginPageComponent,
    HomePageComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { 
 
    constructor(router:Router) {
      router.navigate(['/login']);
    }
  
}
